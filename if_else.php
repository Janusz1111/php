<?php
$a = 7; // przypisujemy wartość zmiennej $a
if ($a % 2 > 0) // reszta z dzielenia przez 2
{
  echo "Liczba nieparzysta";
}
else // brak reszty z dzielenia przez 2 po słowie kluczowym else umieszczamy instrukcje, które wykonają sie w przypadku niespełnienia warunku.
{
  echo "Liczba parzysta";
}
?>