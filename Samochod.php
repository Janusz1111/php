<?php
class Samochod {
  public $marka;
  public $model;
  public $rokProdukcji;
}

//-- Mając przygotowaną definicję klasy można utworzyć nowy, konkretny samochód: 

$samochod = new Samochod();

$samochod->marka = 'Dacial';
$samochod->model = 'Sandero';
$samochod->rokProdukcji = 2021;

echo $samochod->marka . ' ' . $samochod->model . ' ' . $samochod->rokProdukcji;

echo "<br>".$samochod->marka;
$samochod->marka = 'Dacia';
echo "<br>".$samochod->marka . ' ' . $samochod->model . ' ' . $samochod->rokProdukcji;
echo "<br>".$samochod->marka;
?>
<!-- Metody klas mogą zarówno zwracać wartość jak i przyjmować argumenty. również Działają identycznie jak funkcje, z tą dodatkową różnicą,
 że posiadają dostęp do wewnętrznych pól danej klasy.
 Poszczególne pola w obiekcie można modyfikować..
 -->