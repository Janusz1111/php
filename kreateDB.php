<html>
   <head>
      <title>Create a MariaDB Database</title>
   </head>
   <body>
   <div>
   Tworzenie lub usuwanie baz danych w MariaDB wymaga uprawnień nadawanych zwykle tylko użytkownikom root lub administratorom. <br>
   W ramach tych kont masz dwie opcje tworzenia bazy danych - plik binarny mysqladmin i skrypt PHP.<br>
   <b>plik binarny mysqladmin</b><<br>
Przykład użycia pliku binarnego mysqladmin do tworzenia bazy danych o nazwie Products.<br>

[root@host]# mysqladmin -u root -p create PRODUCTS<br>
Enter password:******<br>
   </div>
      <?php
         $dbhost = 'localhost';
         $dbuser = 'root';
         $dbpass = '';
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass);
      
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }

         echo 'Connected successfully<hr />';
         $sql = "CREATE DATABASE PRODUCTS;";
         $retval = mysqli_query($conn, $sql );
      
         if(! $retval ) {
            die('Could not create database: ' . mysqli_error());
         }

         echo "Database PRODUCTS created successfully\n<hr>";
         mysqli_close($conn);
      ?>
   </body>
</html>