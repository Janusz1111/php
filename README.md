# PHP
Programowanie to sztuka a sztuki nie powinno się profanować. Dobrych programistów każdego języka jest mało i ciężko ich znaleźć. Jeśli ktoś jest dobry,  to niezależnie od języka,  w którym programuje,  bez żadnego problemu znajdzie pracę. 
A przeciętniak czy to programujący w PHP,  C++,  czy Javie - może i znajdzie pracę,  ale niekoniecznie tak dobrze płatną.
Staraj się być najlepszym a nie będziesz żałował.
## Krótki opis
Materiał na zajęcia praktyczne z przedmiotu projektowanie stron internetowych dla technika informatyka - kwalifikacja INF.03.
Aby utworzyć nawet prostą ale interaktywną stronę potrzebuhemy treści, która zapewni nam kod HTML, odpowiedniego wyglądu za co odpowiedzialny jest CSS, interakcję z uzytkownikiem zapewni JavaScript a niezbędne operacje  na serwerze uzyskamy dzięki PHP.
Jeszcze tylko trochę SQL , który umożliwi nam magazynowanie i przetwarzanie informacji i strona jest gotowa. Reszta to już Twoja kreatywność, która powinna być nieograniczona czymkolwiek poza uczciwościa i dobrym smakiem.
## trochę trchniki
I jeszcze trochę techniki: pakiet xampp to dobre narzędzie do lokalnego wypróbowanie swoiego dzieła, notepad++ jako prosty edytor do wszystkiego, paint - do prostej obróbli grafiki.
## Opis sktyptów php
Przykładowe kody skryprów na zajęcia praktyczne dla uczniów technikum informatycznego 
**Tabliczka.php** - przykład skryptu zawierającego funkcję do dalszego wykorzystania. Funkcja wywoływana jest w skrypcie **start.php** dzięki metodzie require (wywołanie).  **start.php** + **tabliczka.php** 

Skrypt **formularz.php** przekazuje dane za pomoca formularza **<form>** do skryptu **gora.php** metodą GET. Celemm tego ćwiczenia jest omówienie i pokazanie diałania metody Get. Zwróć uwagę na zastosowanie tej metody i na sposób przekazywania danych. **formularz.php** + **gora.php**. To samo zagadnienie realizuja skrypty: **zadanie.php** i **odp.php** 

Skrypt **login.php** to drugi skrypt wykorzystującu formularz do przesyłania danych - tym razem metodą **POST**. Po podaniu prawidłowego loginu i hasła (są jawnie zapisane w formularzu form.php) uruchamia się skrypt **form.php**, który dodatkowo łaczy się z baza danych i wykonuje proste zaytanie. Celowo wyszukuje drażliwe dane (hasło) aby pokazać konieczność kodowania hasła przetrzymywanego w tabeli bazy danych. Należy zwrócić uwagę sposoby kodowania danych.  **login.php** + **form.php**

Skrypt **hashowanie.php** to wzstęp do omwienia sposobów kodowanie danych w języku PHP. Kilka przykładów zakodowania tego samego hasło (haso wzięte z zadania egzaminacyjnego). Zadaniem ucznia jest określenie (intuicyjnie), która z przedstawionych maetod kodowania jest najskuteczniejsza.
Działanie niektórych z tych skryptów można zobaczyć na mojej szkolnej stronie **zspszczercow.pl/j.przybyl** 
Zapraszam na stronę i proszę o konstruktywne uwagi. 

**Przesyłanie pliku na serwer**. Jest to dosyć niebezpieczna operacja. Należy zabezpieczyćsię na różne okoliczności.
 - Zmienna **$max_rozmiar** ogranicza dopuszczalny rozmiar pliku rgaficznego. Tyle miejscza przeznaczamy na ten plik.
 - Funkcja **is_uploaded_file()** sprawdza czy plik został odebrany.
 - Plik zostaje poddany kilku testom np. czy nie jest za duży, czy typ pliku jest zgodny ze spodziewanym typem;
 - Funkcja  **move_uploaded_file()** zapisuje plik w wybrane miejsce.
 - Ostatnia linujka kodu to polecenie wyświetlenia pliku.
 - Przeanalizuj pliki **nadawca.php** i **odbiorca.php** i przygotój miejsce na serwerze do zapisania plików i miejsca na przesłany plik.

**Rejestracja użytkownika** w bazie danych. pliki **index.php** i **register.php** to kolejna para skryptów. Tym razem uzytkownik przekazuje do bazy danych swoje dane aby zostać dopisany do tabeli uzytkowników. Skrypt i tabela wymagaja wielu  "poprawek" .
- po pierwsze kolumna nr pozwala (a nie powinna) na duplikowanie wartości.
- nie sprawdzamy (a powinniśmy sprawdzać)  czy taki uzytkownik już istnieje w bazie (po co wielokrotnie zapisywać  tego szmego uzytkownika). 
- Cieszę się, że to chce działać - poprawianie to "chleb powszedni" programisty :)


## to będzie rozwijane
W przygotowaniu srtona bloga oraz przykładowe rozwiązanie zadania egzaminacyjnego z kwalifikacji ee.09 (a jak się już pojawią to i z inf.03 - ale nie widzę w tym jakiś istotnych różnic).

## SQL
Plik **uzytkownik** zawiera przyładowe elementy składni języka SQ. Z czasem polecenia dla przykładowej bazy danych będą uzupełniane tak aby zrealizować cztery podstawowe funkcje w aplikacjach korzystających z pamięci trwałej:
**C**reate / utwórz
**R**ead / odczytaj / **r**etrieve / pobierz
**U**pdate / aktualizuj
**D**elete  / usuń
Skrót ten jest również używany w odniesieniu do opisania działań dotyczących oglądania, szukania i zmieniania wszelkich informacji.
## Słownik
- **aplikacja** - program komputerowy.
- **HTML** -język znaczników wykorzystywany do formatowania dokumentów, zwłaszcza stron WWW.
- **CSS** - język służący do opisu formy prezentacji stron WWW.
- **HTTP** - protokół przesyłania dokumentów WWW.
- **GET** - typ żądania HTTP, służący do pobierania zasobów z serwera WWW.
- **POST** - typ żądania HTTP, służący do umieszczania zasobów na serwerze WWW.
- **Kod odpowiedzi HTTP** -numeryczne oznaczenie stanu realizacji zapytania klienta, np. 200 (OK) lub 404 (Not Found). 
- **logowanie** - proces autoryzacji i uwierzytelniania użytkownika w systemie.
- **serwer deweloperski** - testowy serwer www używany w czasie prac nad oprogramowaniem.
- **serwer WWW** - serwer obsługujący protokół HTTP.
- **baza danych** - program przeznaczony do przechowywania i przetwarzania danych.
- **szablon** - wzorzec (nazywany czasem templatką) strony WWW wykorzystywany do renderowania widoków.
- **renderowanie szablonu** - przetwarzanie szkieletowego kodu HTML oraz specjalnych tagów w celu uzyskania kompletnego kodu HTML strony zawierającego przekazane do szablonu dane.
- **ciasteczka** - (ang. cookies) zaszyfrowane dane tekstowe wysyłane przez serwer i zapamiętywane po stronie klienta, zawierają np. identyfikator sesji użytkownika.


## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Janusz1111/php.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/clusters/agent/)

***




## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.



