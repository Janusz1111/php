<html>
   <head>
      <title>Delete a MariaDB Database</title>
   </head>
<div>
Tworzenie lub usuwanie baz danych w MariaDB wymaga uprawnień, zazwyczaj nadawanych tylko użytkownikom root lub administratorom. <br>
W ramach tych kont masz dwie opcje usuwania bazy danych: plik binarny mysqladmin i skrypt PHP.<br>
Uwaga: usunięcie bazy danych jest nieodwracalne, zachowaj ostrożność podczas wykonywania tej operacji. <br>
Skrypt PHP używany do usuwania knot poprosi o potwierdzenie przed usunięciem.<br>
<b>plik binarny mysqladmin</b><br>
Przykład użycia pliku binarnego mysqladmin do usunięcia istniejącej bazy danych.<br>
[root@host]# mysqladmin -u root -p drop PRODUCTS<br>
Enter password:******<br>
mysql> DROP PRODUCTS<br>
ERROR 1008 (HY000): Can't drop database 'PRODUCTS'; database doesn't exist<br>
<div>
   <body>
      <?php
         $dbhost = 'localhost';
         $dbuser = 'root';
         $dbpass = '';
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass);
      
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }
         echo 'Connected successfully<hr>';
         
         $sql = 'DROP DATABASE PRODUCTS1';
         $retval = mysqli_query( $conn, $sql);
         
         if(! $retval ){
            die('Could not delete database: ' . mysqli_error());
         }

         echo "Database PRODUCTS deleted successfully\n<hr>";
         mysqli_close($conn);
      ?>
   </body>
</html>