
<?php
   $dbhost = 'localhost'; 
   $dbuser = 'root';
   $dbpass = ''; 
   $conn = mysqli_connect($dbhost, $dbuser, $dbpass); 
   $nameDB = 'PRODUCTS';
   if(! $conn ) {
      die('Could not connect: ' . mysqli_error());
   }

   $sql = 'SELECT product_id, product_name, product_manufacturer, submission_date FROM products_tbl;'; 
   mysqli_select_db($conn, $nameDB );
   $retval = mysqli_query( $conn, $sql);
   if(! $retval ) { die('Could not get data: ' . mysqli_error()); 
   } 
   while($row = mysqli_fetch_array($retval)) { 
   echo "Product ID :{$row[0]} <br> ".
         "Name: {$row[1]} <br> ". 
		 "Manufacturer: {$row[2]} <br> ".
         "Ship Date : {$row[3]} <br> ". 
		 "--------------------------------<br>"; 
		 } 
	mysqli_free_result($retval);
	echo "Pobieranie danych zakończone sukcesem.\n<hr>";
	mysqli_close($conn);
?>