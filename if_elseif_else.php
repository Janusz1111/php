<?php
$a = 34; // przypisujemy wartość zmiennej $a

if ($a%8 == 0) // Jeżeli liczba podzielna jest przez osiem
  echo "Liczba podzielna przez osiem";

elseif ($a%4 == 0) // a może liczba jest podzielna przez cztery ale nie przez osiem?
  echo "Liczba podzielna przez 4, ale nie przez 8";

elseif ($a%2 == 0) // a może liczba podzielna przez dwa ale nie przez cztery lub osiem
  echo "Liczba podzielna przez 2, ale nie przez 4";

else // żadna z powyższych cyli nieparzysta
  echo "Liczba nieparzysta";
?>