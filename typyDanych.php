<html>
   <head>
      <title>Connect to MariaDB Server</title>
   </head>

   <body>
   <h1> Typy danych w MariaDB</h1>
Dobre definicje pól są niezbędne do optymalizacji bazy danych. Idealne podejście wymaga użycia wyłącznie pola wymaganego typu i rozmiaru. <br>
Na przykład, jeśli będziesz używać tylko pola o szerokości pięciu znaków, nie definiuj pola o szerokości 20 znaków. <br>
Typy pól (lub kolumn) są również znane jako typy danych, biorąc pod uwagę typy danych przechowywane w polu.<hr>

Typy danych MariaDB można kwalifikować jako wartości liczbowe, daty i godziny oraz wartości łańcuchowe.<br>

<strong><u>Numeryczne typy danych</u></strong><hr><br>
<strong>TINYINT </strong>- Ten typ danych reprezentuje małe liczby całkowite mieszczące się w zakresie ze znakiem od -128 do 127 oraz z zakresu bez znaku od 0 do 255.
<br>
<strong>BOOLEAN</strong> - Ten typ danych wiąże wartość 0 z „fałszem”, a wartość 1 z „prawdą”.
<br>
<strong>SMALLINT </strong>- Ten typ danych reprezentuje liczby całkowite w zakresie ze znakiem od -32768 do 32768 oraz z zakresu bez znaku od 0 do 65535.
<br>
<strong>MEDIUMINT</strong> - Ten typ danych reprezentuje liczby całkowite w zakresie ze znakiem od -8388608 do 8388607 oraz z zakresu bez znaku od 0 do 16777215.
<br>
<strong>INT(also INTEGER)</strong>- Ten typ danych reprezentuje liczbę całkowitą o normalnym rozmiarze. Zakres oznaczony jako bez znaku obejmuje zakres od 0 do 4294967295. Po podpisaniu (ustawienie domyślne) zakres obejmuje od -2147483648 do 2147483647. Gdy kolumna jest ustawiona na ZEROFILL (stan bez znaku), wszystkie jej wartości są poprzedzone zerami w celu umieszczenia M cyfr w wartości INT.
<br>
<strong>BIGINT </strong>- Ten typ danych reprezentuje liczby całkowite z zakresu ze znakiem od 9223372036854775808 do 9223372036854775807 oraz z zakresu bez znaku od 0 do 18446744073709551615.
<br>
<strong>DECIMAL(także DEC, NUMERIC, FIXED)</strong> - Ten typ danych reprezentuje dokładne liczby stałoprzecinkowe, gdzie M określa jego cyfry, a D określa cyfry po przecinku. Wartość M nie dodaje „-” ani kropki dziesiętnej. Jeśli D jest ustawione na 0, nie pojawia się żadna część dziesiętna ani ułamkowa, a wartość zostanie zaokrąglona do najbliższego DECIMAL podczas WSTAWIANIA. Maksymalna dozwolona liczba cyfr to 65, a maksymalna dla miejsc po przecinku to 30. Wartość domyślna dla M w przypadku pominięcia to 10, a 0 dla D w przypadku pominięcia.
<br>
<strong>FLOAT</strong> - Ten typ danych reprezentuje małą, zmiennoprzecinkową liczbę o wartości 0 lub liczbie z następujących zakresów -
<br>
-3,402823466E + 38 do -1,175494351E-38
<br>
Od 1.175494351E-38 do 3.402823466E + 38
<br>
<strong>DOUBLE (również REAL i DOUBLE PRECISION)</strong> - Ten typ danych reprezentuje liczby zmiennoprzecinkowe o normalnym rozmiarze o wartości 0 lub z następujących zakresów -
<br>
-1,7976931348623157E + 308 do -2,2250738585072014E-308
<br>
2.2250738585072014E-308 do 1.7976931348623157E + 308
<br>
<strong>BIT</strong>- Ten typ danych reprezentuje pola bitowe, gdzie M określa liczbę bitów przypadających na wartość. W przypadku pominięcia M wartością domyślną jest 1. Wartości bitów można zastosować z „b '[wartość]'”, w którym wartość reprezentuje wartość bitu w 0 i 1 sekundach. Wypełnienie zerowe następuje automatycznie od lewej strony na całej długości; na przykład „10” staje się „0010”.
<br>
<strong><u>Typy danych daty i godziny</u></strong><hr>
<br>
<strong>DATE </strong>- Ten typ danych reprezentuje zakres dat od „1000-01-01” do „9999-12-31” i używa formatu daty „RRRR-MM-DD”.
<br>
<strong>TIME</strong> - Ten typ danych reprezentuje zakres czasu od „-838: 59: 59,999999” do „838: 59: 59,999999”.
<br>
<strong>DATETIME</strong>- Ten typ danych reprezentuje zakres od „1000-01-01 00: 00: 00.000000” do „9999-12-31 23: 59: 59.999999”. Używa formatu „RRRR-MM-DD GG: MM: SS”.
<br>
<strong>TIMESTAMP</strong>- Ten typ danych reprezentuje znacznik czasu w formacie „RRRR-MM-DD GG: MM: DD”. Znajduje zastosowanie głównie w szczegółowym określaniu czasu modyfikacji bazy danych, np. Wstawienia lub aktualizacji.
<br>
<strong>YEAR</strong>- Ten typ danych przedstawia rok w formacie 4-cyfrowym. Format czterocyfrowy dopuszcza wartości z zakresu od 1901 do 2155 i 0000.
<br>
<strong><u>String DataTypes</u><strong><hr>
<br>

<strong>String literals</strong>- Ten typ danych reprezentuje sekwencje znaków ujęte w cudzysłowy.
<br>
<strong>CHAR</strong>- Ten typ danych reprezentuje ciąg o stałej długości, z prawej strony, zawierający spacje o określonej długości. M reprezentuje długość kolumny zawierającą znaki z zakresu od 0 do 255, jej domyślna wartość to 1.
<br>
<strong>VARCHAR </strong>- Ten typ danych reprezentuje ciąg o zmiennej długości z zakresem M (maksymalna długość kolumny) od 0 do 65535.
<br>
<strong>BINARY</strong> - Ten typ danych reprezentuje binarne ciągi bajtów, z M jako długością kolumny w bajtach.
<br>
<strong>VARBINARY</strong> - Ten typ danych reprezentuje binarne ciągi bajtów o zmiennej długości, z M jako długością kolumny.
<br>
<strong>TINYBLOB</strong>- Ten typ danych reprezentuje kolumnę typu blob o maksymalnej długości 255 (28-1) bajtów. W pamięci każdy używa jednobajtowego przedrostka długości wskazującego ilość bajtów w wartości.
<br>
<strong>BLOB</strong>- Ten typ danych reprezentuje kolumnę typu blob o maksymalnej długości 65 535 (216 - 1) bajtów. W pamięci każdy używa dwubajtowego przedrostka długości wskazującego ilość bajtów w wartości.
<br>
<strong>MEDIUMBLOB</strong>- Ten typ danych reprezentuje kolumnę typu blob o maksymalnej długości 16 777 215 (22 4 - 1) bajtów. W pamięci każdy używa przedrostka o długości trzech bajtów, wskazującego ilość bajtów w wartości.
<br>
<strong>LONGBLOB</strong>- Ten typ danych reprezentuje kolumnę typu blob o maksymalnej długości 4294 967 295 (2 32 - 1) bajtów. W pamięci każdy używa czterobajtowego prefiksu długości wskazującego ilość bajtów w wartości.
<br>
<strong>TINYTEXT</strong>- Ten typ danych reprezentuje kolumnę tekstową o maksymalnej długości 255 (2 8 - 1) znaków. W pamięci każdy używa jednobajtowego prefiksu długości wskazującego ilość bajtów w wartości.
<br>
<strong>TEXT</strong>- Ten typ danych reprezentuje kolumnę tekstową o maksymalnej długości 65 535 (2 16 - 1) znaków. W pamięci każdy używa dwubajtowego przedrostka długości wskazującego ilość bajtów w wartości.
<br>
<strong>MEDIUMTEXT</strong>- Ten typ danych reprezentuje kolumnę tekstową o maksymalnej długości 16 777 215 (2 24 - 1) znaków. W pamięci każdy używa przedrostka o długości trzech bajtów, wskazującego ilość bajtów w wartości.
<br>
<strong>LONGTEXT</strong>- Ten typ danych reprezentuje kolumnę tekstową o maksymalnej długości 4 294 967 295 lub 4 GB (2 32 - 1) znaków. W pamięci każdy używa czterobajtowego prefiksu długości wskazującego ilość bajtów w wartości.
<br>
<strong>ENUM</strong> - Ten typ danych reprezentuje obiekt typu string mający tylko jedną wartość z listy.
<br>
<strong>SET</strong>- Ten typ danych reprezentuje obiekt łańcuchowy mający zero lub więcej wartości z listy, z maksymalnie 64 elementami. Wartości SET występują wewnętrznie jako wartości całkowite.
   </body>
</html>