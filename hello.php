<?php
if($argc != 2) {
    echo "Usage: php hello.php [imię].\n";
    exit(1);
}
$imie = $argv[1];
echo "Witaj, $imie!\n";
//Wykonaj następujące czynności w celu uruchomienia tego skryptu:
//1 - Otwórz terminal (wiersz poleceń) w folderze w którym zapisany jest skrypt
//2 - Upewnij się, że masz prawidłowo zainstalowamy PHP - wydaj np. polecenie PHP -v 
// gdy zobaczysz napis z wersją PHP wszysttko jest OK
//3 - W terminalu podaj następujące polecenie: php hello.php imię
//Zaskoczony?  Tylko nie pomyl folderu.
//Jeżeli otwierasz wiersz poleceń (terminal) w innej lokalizacji to po  php musisz podać względną ścieżką do pliku, który chcesz uruchomić.
// Np. wiersz poleceń uruchomiony masz w folderze xampp a plik hello.php jest w podfolderze htdocs
//W tym przypadku podajesz polecenie o następującej skłądn: php htdocs/hello.php Imię