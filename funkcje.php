<html>
   <head>
   <meta charset="utf-8">
      <title>Wybrane funkcje mariaDB</title>
	  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
   </head>

   <body>
   <div>
   <table class="table-primary">
   <tr class="table-info">
      <td class="table-info">Lp.</td><td class="table-secondary">funkcja()</td>   <td class="table-success">działanie</td>  <td class="table-warning"> przykład</td>
   </tr>
   <tr class="table-info">
      <td class="table-info">1</td><td class="table-secondary">COUNT</td>   <td class="table-success">Zlicza liczbę rekordów.</td>  <td class="table-warning"> WYBIERZ LICZBĘ (*) Z tabeli customer_table;</td>
   </tr>

1	




Example -

2	
MIN

Ujawnia minimalną wartość zbioru rekordów.

Example - WYBIERZ organizację, MIN (konto) Z umów GRUPA WEDŁUG organizacji;

3	
MAX

Ujawnia maksymalną wartość zbioru rekordów.

Example - WYBIERZ organizację, MAX (account_size) FROM kontraktów GROUP BY organizacja;

4	
AVG

Oblicza średnią wartość zbioru rekordów.

Example - WYBIERZ AVG (account_size) Z umów;

5	
SUM

Oblicza sumę zbioru rekordów.

Example - SUMA WYBORU (account_size) z umów;
   </table>
   </div>
   </body>
</html>