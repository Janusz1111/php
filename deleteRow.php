<html>
   <head>
   <meta charset="utf-8">
      <title>Zapytanie w MariaDB</title>
   </head>

   <body>
   <div>
   <h1>MariaDB - kwerenda - wstawianiedanych do tabeli </h1>
Polecenie <strong>DELETE</strong> usuwa wiersze tabeli z określonej tabeli oraz zwraca ilość usuniętych wierszy (danych).<br> 
Dostęp do ilości tych usunietych danych umozliwia funkcja <strong>ROW_COUNT ()</strong>. Klauzula <strong>WHERE </strong>określa wiersze do usunięcia, a w przypadku jej braku wszystkie wiersze są usuwane. <br>
Klauzula <strong>LIMIT</strong> kontroluje liczbę usuniętych wierszy.<br>
W instrukcji <strong>DELETE</strong> dla wielu wierszy usuwane są tylko te wiersze, które spełniają warunek. Klauzule <strong>LIMIT i WHERE </strong>są niedozwolone. <br>
Instrukcje <strong>DELETE</strong> umożliwiają usuwanie wierszy z tabel w różnych bazach danych, ale nie pozwalają na usuwanie z tabeli, a następnie wybieranie z tej samej tabeli w podzapytaniu.<br>
Przykład: <br>
DELETE FROM table_name [WHERE …]<br>
Plecenie DELETE  mozna wykonać z wiersza poleceń lub za pomocą skryptu PHP.
   </div>
<?php


   $dbhost = 'localhost'; 
   $dbuser = 'root';
   $dbpass = ''; 
   $conn = mysqli_connect($dbhost, $dbuser, $dbpass); 
   $nameDB = 'PRODUCTS';
   if(! $conn ) {
      die('Could not connect: ' . mysqli_error());
   }
	mysqli_select_db($conn, $nameDB );
	
   	$sql = "DELETE FROM `products_tbl` WHERE `product_id` = 13;";
  
   
   $retval = mysqli_query( $conn, $sql);
   
   if(! $retval ) { die('Could not get data: ' . mysqli_error()); 
   } 
   
	echo "Usunięcie danych zakończone sukcesem.\n<hr>";
	mysqli_close($conn);
?>