<?php
// --- klasa bazowa
class KlasaBaza
{
public function napisz()
{
echo 'Tekst pochodzi z klasy bazowej';
}
}
// --- Klasa potomna
class KlasaPotomna extends KlasaBaza
{
public function napisz()
{
echo "<br>";
parent::napisz();
echo ' - wywołany z klasy potomnej...';
echo '<br>Tekst pochodzi z klasy potomnej';
}
}
$objA=new KlasaBaza();
$objB=new KlasaPotomna();
echo $objA->napisz();
echo "<br>";
echo $objB->napisz();
echo "<br>";
?>
?>
