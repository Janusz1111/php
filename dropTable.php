<html>
   <head>
      <title>Create a MariaDB Table</title>
   </head>

   <body>
   <div>
   <h1>MariaDB - usunięcie tabeli</h1>
   <b>Uwaga</b> - usunięcie tabeli to operacja nieodwracalna. Ogólna składnia usuwania tabeli jest następująca <br>

DROP TABLE table_name ;<br>
Istnieją dwie opcje usuwania tabeli: poprzez wiersz poleceń lub skrypt PHP.

   </div>
      <?php
         $dbhost = 'localhost';
         $dbuser = 'root';
         $dbpass = '';
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass);
		 $nameDB = 'PRODUCTS';
		 $tableName = "products_tbl";
      
         if(! $conn ){
            die('Could not connect: ' . mysqli_error());
         }
         echo 'Connected successfully<hr>';
          mysqli_select_db($conn, $nameDB );
		  
		$sql = "DROP TABLE $tableName;"; 
		$retval = mysqli_query(  $conn, $sql);
      
         if(! $retval ) { die('Could not delete table: ' . mysql_error()); }
         echo "Table  $tableName deleted successfully \n<hr>";
         
         mysqli_close($conn);
      ?>
   </body>
</html>