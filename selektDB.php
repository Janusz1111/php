<html>
   <head>
      <title>Select a MariaDB Database</title>
   </head>

   <body>
   <div>
Po nawiązaniu połączenia z MariaDB należy wybrać bazę danych do pracy, ponieważ może istnieć wiele baz danych. <br>
Zadanie to można wykonać na dwa sposoby: z wiersza poleceń lub za pomocą skryptu PHP.<br>
<b>Wiersz polecenia</b><br>
Wybierając bazę danych w wierszu poleceń, po prostu użyj polecenia SQL ‘use’ <br>

[root@host]# mysql -u root -p<br>

Enter password:******<br>

mysql> use PRODUCTS;<br>

Database changed<br>

mysql> SELECT database();  <br>
+-------------------------+ <br>
| Database                | <br>
+-------------------------+ <br>
| PRODUCTS                | <br>
+-------------------------+<br>
Po wybraniu bazy danych wszystkie kolejne polecenia będą działały na wybranej bazie danych.<br>

<b>Uwaga</b>- Wszystkie nazwy (np. Baza danych, tabela, pola) uwzględniają wielkość liter. Upewnij się, że polecenia są zgodne z właściwym przypadkiem
   </div>
      <?php
         $dbhost = 'localhost';
         $dbuser = 'root';
         $dbpass = '';
		 $dbname = 'PRODUCTS2';
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass);
      
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }
         echo 'Connected successfully<hr>';
         
         mysqli_select_db(  $conn ,  $dbname );
		 echo "Wybrano baz dantcy  $dbname"."<hr>";
         mysqli_close($conn);
      ?>
   </body>