<html>
   <head>
      <title>Zapytanie w MariaDB</title>
   </head>

   <body>
   <div>
   <h1>MariaDB - kwerenda - wstawianiedanych do tabeli </h1>
Wstawienie danych do tabeli wymaga polecenia INSERT. Ogólna składnia polecenia to <br>
INSERT, po której następuje nazwa tabeli, pola i wartości.<br>
Ogólna składnię polecenia INSERT to:<br>

INSERT INTO tablename (field,field2,...) VALUES (value, value2,...);<br>
Instrukcja wymaga użycia pojedynczych lub podwójnych cudzysłowów dla wartości łańcuchowych. <br>
Inne opcje instrukcji obejmują instrukcje „INSERT ... SET”, instrukcje „INSERT ... SELECT” i kilka innych opcji.<br>

<b>Uwaga</b> - Funkcja VALUES (), która pojawia się w instrukcji, ma zastosowanie tylko do instrukcji INSERT i zwraca NULL, jeśli zostanie użyta w innym miejscu.<br>

Operację można wykonać używając wiersza poleceń lub skryptu PHP. W przypadku skryptu PHP dane mozemy zapisać bezpośrednio w zapytaniu albo przesłąć do bazy danych za pomocą formularza.<br>
   <form  action = "kwerenda.php"  method="post">
   Produkt:<input type = "text" name="product_name"><br>
   Producent:<input type = "text" name="product_manufacturer"><br>
   Data: <input type = "date" name="ship_date"><br>
   <input type = "submit" walue="Prześlij"name= "add">
   </form >
   </div>
<?php
   
      $dbhost = 'localhost';
      $dbuser = 'root';
      $dbpass = '';
      $conn = mysqli_connect($dbhost, $dbuser, $dbpass);
	  $dbname = 'PRODUCTS';
	  
      if(! $conn ) {
         die('Could not connect: ' . mysqli_error());
      }

      $product_name = addslashes ($_POST['product_name']); //zwraca ciąg znaków z odwrotnymi ukośnikami przed predefiniowanymi znakami
      $product_manufacturer = addslashes ($_POST['product_name']);
      $ship_date = $_POST['ship_date'];
	 
	  	  
      $sql = "INSERT INTO `products_tbl`(`product_id`, `product_name`, `product_manufacturer`, `submission_date`) VALUES ('','$product_name','$product_manufacturer','$ship_date ');";
	  mysqli_select_db($conn, $dbname);
      $retval = mysqli_query( $conn, $sql);
      
      if(! $retval ) {
         die('Could not enter data: ' . mysqli_error());
      }

      echo "Entered data successfully\n<hr>";
      mysqli_close($conn);
   
?>