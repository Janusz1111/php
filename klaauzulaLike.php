<html>
   <head>
      <title>Zapytanie w MariaDB</title>
   </head>

   <body>
   <div>
   <h1>MariaDB - kwerenda - wstawianiedanych do tabeli </h1>
Polecenie DELETE usuwa wiersze tabeli z określonej tabeli i zwraca ilość usuniętych wierszy (danych).<br> 
Dostęp do ilości ych danych umozliwia funkcja ROW_COUNT (). Klauzula WHERE określa wiersze, aw przypadku jej braku wszystkie wiersze są usuwane. <br>
Klauzula LIMIT kontroluje liczbę usuniętych wierszy.<br>
W instrukcji DELETE dla wielu wierszy usuwa tylko te wiersze, które spełniają warunek.<br>
klauzule LIMIT i WHERE są niedozwolone. <br>
Instrukcje DELETE umożliwiają usuwanie wierszy z tabel w różnych bazach danych, ale nie pozwalają na usuwanie z tabeli, a następnie wybieranie z tej samej tabeli w podzapytaniu.<br>
Przykład: <br>
DELETE FROM table_name [WHERE …]<br>
Plecenie DELETE  mozna wykonać z wiersza poleceń lub za pomocą skryptu PHP.
   </div>
<?php
   $dbhost = 'localhost'; 
   $dbuser = 'root';
   $dbpass = ''; 
   $conn = mysqli_connect($dbhost, $dbuser, $dbpass); 
   $nameDB = 'PRODUCTS';
   if(! $conn ) {
      die('Could not connect: ' . mysqli_error());
   }
	mysqli_select_db($conn, $nameDB );
	
   	$sql = 'SELECT product_id, product_name, product_manufacturer, submission_date FROM products_tbl WHERE product_manufacturer LIKE "xyz%";';
   
   $retval = mysqli_query( $conn, $sql);
   
   if(! $retval ) { die('Could not get data: ' . mysqli_error()); 
   } 
   while($row = mysqli_fetch_array($retval, MYSQL_ASSOC)) {
      echo "Product ID:{$row['product_id']} <br> ".
         "Produkt: {$row['product_name']} <br> ".
         "Producent: {$row['product_manufacturer']} <br> ".
         "Data : {$row['ship_date']} <br> ".
         "--------------------------------<br>";
   }
	echo "wyszukiwanie zakończone sukcesem.\n<hr>";
	mysqli_close($conn);
?>