<form action="" method="GET">
<input type="text"  name="wzrost" placeholder="wzrost" value="1.85">
<input type="text"  name="waga" placeholder="waga" value="130">
<input type="submit" value="przelicz">
</form>
<?php
$opis = <<<TEXT
Program oblicza wartość współczynnika BMI (ang. body mass index) 
wg. wzoru: BMI=waga/wzrost2 (wzr0st w [m] waga w [kg]). <br>
Jeśli wynik jest w przedziale (18,5 - 24,9) to widzisz napis ”waga prawidłowa”, <br>
jeżeli poniżej normy to widzisz napis ”niedowaga”, <br>
jeżeli powyżej normy to widzisz napis ”nadwaga”.<br>
Ponadto widzisz wartość swojego BMI i zalecaną dla Twojego wzrostu wagę.<br>
Ten indeks traktuj "<b>z przymróżeniem oka</b>". <br>
Aby lepiej obliczyć zalecana wagę należy uwzgladnić Twój wiek, twoją płeć, tryb życia, rodzaj aktywniści fizycznej, Twoją budowe.<br>
A wynik i tak będzie tylko orientacyjny.<hr>
TEXT;
echo $opis;
$waga=$_GET["waga"];
$wzrost=$_GET["wzrost"];
$BMI = $waga/($wzrost*$wzrost);

$wagaMin=18.5*($wzrost*$wzrost);
$wagaMax=24.9*($wzrost*$wzrost);

if ($BMI < 18.5) echo "niedowaga. <br>Twoje bmi to: ".$BMI." Powinieneś ważyć minimu " .$wagaMin;
if ($BMI > 24.9) echo "nadwaga. <br>Twoje bmi to: ".$BMI." Powinieneś ważyć maksymalnie " .$wagaMax;
if ($BMI>=18.5 and $BMI <=24.9) echo "waga prawidłowa. <br>Twoje bmi to: ".$BMI;
?>
